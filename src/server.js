const dotenv = require('dotenv')
const app = require('./app')

dotenv.config()

app.listen(process.env.PORT || 3333, () => {
  console.log('Server is running!')
})
