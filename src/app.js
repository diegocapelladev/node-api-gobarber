const express = require('express')
require('express-async-errors')
const cors = require('cors')
const path = require('path')
const Youch = require('youch')

require('dotenv').config()

const Sentry = require('@sentry/node')

const sentryConfig = require('./config/sentry')

const routes = require('./routes')
require('./database')

class App {
  constructor() {
    this.server = express()

    Sentry.init(sentryConfig)
    this.server.use(Sentry.Handlers.requestHandler())

    this.middlewares()
    this.routes()
    this.exceptionHandler()
  }

  middlewares() {
    this.server.use(express.json())
    this.server.use(cors({ credentials: true, origin: `${process.env.CORS}` }))
    this.server.use(
      '/files',
      express.static(path.resolve(__dirname, '..', 'tmp', 'uploads'))
    )
  }

  routes() {
    this.server.use(routes)
    this.server.use(Sentry.Handlers.errorHandler())
  }

  exceptionHandler() {
    this.server.use(async (err, req, res, next) => {
      if (process.env.NODE_ENV === 'development') {
        const errors = await new Youch(err, req).toJSON()

        return res.status(500).json(errors)

        return res.status(500).json({ error: 'Internal server error.' })
      }
    })
  }
}

module.exports = new App().server
