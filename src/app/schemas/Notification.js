const mongoose = require('mongoose')

const NotificationSchema = new mongoose.Schema(
  {
    content: {
      type: String,
      require: true
    },
    user: {
      type: Number,
      require: true
    },
    read: {
      type: Boolean,
      require: true,
      default: false
    }
  },
  {
    timestamps: true
  }
)

module.exports = mongoose.model('Notification', NotificationSchema)
